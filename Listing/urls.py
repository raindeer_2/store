from django.urls import path

from .views import product_list, product_detail,search_product
app_name = 'Listing'

urlpatterns = [

    path('', product_list, name='product_list'),
    path('search',search_product,name='product_search'),
    path('<slug:category_slug>', product_list, name='product_list_by_category'),
    path('<slug:category_slug>/<slug:product_slug>', product_detail, name='product_detail'),
]
