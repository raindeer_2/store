from django.shortcuts import render
from django.shortcuts import get_object_or_404, redirect
from .models import Category, Product, Review
from cart.forms import CartAddProductForm

from django.db.models import Q
from django.views.generic import ListView

categories = Category.objects.all()


def product_list(request, category_slug=None):
    if category_slug:
        request_category = get_object_or_404(Category, slug=category_slug)
        products = Product.objects.filter(category=request_category)
    else:
        # if request.user.is_authenticated:
        request_category = None
        products = Product.objects.all()

    return render(request, 'product/list.html',
                  {'categories': categories,
                   'request_category': request_category,
                   'products': products})


def product_detail(request, category_slug, product_slug):
    category = get_object_or_404(Category, slug=category_slug)
    product = get_object_or_404(Product, category_id=category.id, slug=product_slug)
    if request.method == 'POST':
        review_form = ReviewForm(request.POST)

        if review_form.is_valid():
            cf = review_form.cleaned_data
            author_name = "Anon"
            Review.objects.create(product=product, author=author_name, rating=cf['rating'], text=cf['text'])

        return redirect('Listing:product_detail', category_slug=category_slug, product_slug=product_slug)
    else:

        cart_product_form = CartAddProductForm()

    return render(request, 'product/detail.html', {'product': product, 'cart_product_form': cart_product_form})


def search_product(request):
    if request.method == "POST":
        searched = request.POST['searched']
        products = Product.objects.filter(name__contains=searched)
        return render(request, 'product/search.html', {'searched': searched, 'products': products})
    else:
        return render(request, 'product/search.html')
