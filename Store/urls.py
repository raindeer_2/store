from django.contrib import admin
from django.urls import path,include
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.auth import logout
if settings.DEBUG:
    urlpatterns = [
        path('ticket/', include('support.urls', namespace='ticket')),
        path('api/',include('api.urls', namespace='ticketss')),
        path('login/', include('social_django.urls', namespace='social')),

        path('auth1/', include('djoser.urls')),
        path('auth2/', include('djoser.urls.jwt')),

        path("account/", include("account.urls")),
        path('admin/', admin.site.urls),
        path('cart/', include('cart.urls', namespace='cart')),
        path('orders/', include('orders.urls', namespace='orders')),

        path('', include('Listing.urls', namespace='listings')),

    ]
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)