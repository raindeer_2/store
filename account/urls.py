from . import views
from rest_framework import routers
app_name = 'account'

router = routers.SimpleRouter()

router.register('all-profiles', views.UserProfileListCreateView, basename='all-profiles')
router.register('profile/<int:pk>',views.UserProfileDetailView, basename='profile')

urlpatterns = router.urls
