from django.urls import path
from . import views
from rest_framework import routers
app_name = 'ticketss'

router = routers.SimpleRouter()

router.register('ticket', views.TicketViewSet, basename='create')
router.register('message',views.MessageViewSet, basename='message')
router.register('product',views.ProductViewSet,basename='product')
router.register('category',views.CategoryViewSet,basename='category')
router.register('order',views.OrderViewSet,basename='order')
urlpatterns = router.urls
