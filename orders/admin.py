from django.contrib import admin
from .models import Order, OrderItem


# Register your models here.
class OrderItemInline(admin.TabularInline):
    model = OrderItem
    raw_id_fields = ['product']


class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'first_name', 'last_name', 'email',
                    'address', 'postal_code', 'city', 'paid',
                    'created', 'updated']
    list_filter = ['paid', 'created', 'updated']
    inlines = [OrderItemInline]
    actions = ['make_not_paid', 'make_paid']

    @admin.action(description='Mark as paid')
    def make_paid(self, request, queryset):
        queryset.update(paid='paid')

    @admin.action(description='Mark  as not paid')
    def make_not_paid(self, request, queryset):
        queryset.update(paid='not paid')


admin.site.register(Order, OrderAdmin)
