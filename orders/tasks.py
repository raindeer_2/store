from celery import shared_task
from django.core.mail import send_mail
from .models import Order



@shared_task
def order_created(order_id):
    order = Order.objects.get(id=order_id)
    print('some message')

    subject = f'Order nr. {order.id}'
    message = f'Dear {order.last_name}'
    send_mail(message, subject,
              'test.django.mail.send.mail@gmail.com',
              [order.email],
              fail_silently=False)
