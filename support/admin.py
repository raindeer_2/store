from django.contrib import admin
from .models import Tickets


# Register your models here.

@admin.register(Tickets)
class TicketAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_name', 'last_name', 'email', 'problem', 'status')
    list_filter = ['status']
    actions = ['make_not_decided', 'make_decided', 'make_frozen']

    @admin.action(description='Mark not decided')
    def make_not_decided(self, request, queryset):
        queryset.update(status='not decided')

    @admin.action(description='Mark  decided')
    def make_decided(self, request, queryset):
        queryset.update(status='decided')

    @admin.action(description='Mark frozen')
    def make_frozen(self, request, queryset):
        queryset.update(status='frozen')
