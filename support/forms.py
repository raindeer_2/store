from django import forms
from .models import Tickets

class TicketCreateForm(forms.ModelForm):
    class Meta:
        model=Tickets
        fields=['first_name','last_name','email','problem']