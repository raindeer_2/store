from django.db import models


# Create your models here.
class Tickets(models.Model):
    first_name = models.CharField(max_length=60)
    last_name = models.CharField(max_length=60)
    email = models.EmailField()
    problem = models.TextField()
    status = models.CharField(max_length=255, default='unknown')

    def __str__(self):
        return self.first_name, self.problem


class Message(models.Model):
    status=models.CharField(max_length=255,default='qwe')
    messages = models.CharField(max_length=255)

    def __str__(self):
        return  self.messages
