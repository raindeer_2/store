from celery import shared_task
from django.core.mail import send_mail
from .models import Tickets



@shared_task
def ticket_created(order_id):
    ticket = Tickets.objects.get(id=order_id)
    print('some message')

    subject = f'Ticket nr. {ticket.id}'
    message = f'Dear {ticket.first_name},your {ticket.problem} in process '
    send_mail(message, subject,
              'test.django.mail.send.mail@gmail.com',
              [ticket.email],
              fail_silently=False)