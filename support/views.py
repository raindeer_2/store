from django.shortcuts import render

# Create your views here.
from django.core.mail import send_mail
from .forms import TicketCreateForm
from .models import Tickets
from .tasks import ticket_created


def ticket_create(request):
    ticket = Tickets(request)
    sent = False
    if request.method == 'POST':
        form = TicketCreateForm(request.POST)
        if form.is_valid():
            ticket = form.save()
            ticket_created.delay(ticket.id)
        return render(request, 'support/created.html', {'ticket': ticket})
    else:
        form = TicketCreateForm()
    return render(request, 'support/create.html', {'form': form})
